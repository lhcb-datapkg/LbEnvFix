# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased][]

## [v1r7][] - 2022-09-29
### Changed
- Make host-os override in LD_LIBRARY_PATH with target-os more robust (!10)

## [v1r6][] - 2021-01-15
### Changed
- Automatically deploy releases to CVMFS (!9)
- Update version of xrootd to 4.10.0 on selected platforms (LHCBPS-1990, !8)

## [v1r5][] - 2020-05-05
### Fixed
- Remove DIRACOS from application environments (LHCBPS-1855, !7)

## [v1r4][] - 2019-02-04
### Changed
- Use XRootD 3.2.7 instead of 3.1.02p for slc5-gcc46

## [v1r3][] - 2018-12-11
### Changed
- Use CompatSys/IgnoreExitCode for LHCb v39r1p3 (as well as for v39r1 and v39r1p1) (!5)

## [v1r2][] - 2018-10-01
### Changed
- Use CompatSys/IgnoreExitCode for LHCb v39r1p1 (as well as for v39r1) (!4)

## [v1r1][] - 2018-05-14
### Changed
- Use XRootD 4.8.2 instead of 4.x for available platforms (!3)

## [v1r0][] - 2018-03-15
### Added
- Override (first) host-os entry in LD_LIBRARY_PATH with target-os (!2)
- [Change log](CHANGELOG.md)

### Fixed
- Fixed xrootd tests (!1)

## v0r0 - 2017-11-10
### Added
- First release

[Unreleased]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/compare/v1r6...master
[v1r6]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r6
[v1r5]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r5
[v1r4]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r4
[v1r3]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r3
[v1r2]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r2
[v1r1]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r1
[v1r0]: https://gitlab.cern.ch/lhcb-datapkg/LbEnvFix/-/releases/v1r0
