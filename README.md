LbEnvFix
========

This project contains metadata files that allow running "old" LHCb software stacks.
For example, the version of xrootd initially used is overriden by a newer one
compatible with the latest server releases.


Format of the `override_map.json` file
------------------------------------

The `override_map.json` file contain the rules to override the content of the
environment.

It must contain an object with, at least, the two fields

-   _version_: a version identifier
-   _substitutions_: the list of substitution rules

The _substitutions_ field is a list of lists, with the sublists made of 2 or 3
elements:

-   regular expression to replace
-   replacement string
-   (optional) comment describing the purpose of the rule

The regular expression and the replacement string follow strictly the rules of
[Python re.sub function](https://docs.python.org/2/library/re.html#re.sub).

The replacement rules should expect to operate on strings of the format:

    variable_name=variable_value

and should modify the value without altering the variable name.
