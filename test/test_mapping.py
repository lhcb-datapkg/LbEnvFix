#!/usr/bin/env python
from __future__ import print_function

import os
import re
import json

MAP_FILE = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                        'override_map.json')

COMPAT_ROOT = '/cvmfs/lhcb.cern.ch/lib/lhcb/COMPAT/COMPAT_v1r19'


def test_format():
    json.load(open(MAP_FILE, 'rb'))


def test_fields():
    data = json.load(open(MAP_FILE, 'rb'))
    assert 'version' in data
    assert 'substitutions' in data
    for entry in data['substitutions']:
        assert len(entry) >= 2


def test_regex_syntax():
    'regular expression syntax'
    data = json.load(open(MAP_FILE, 'rb'))
    print()
    for entry in data['substitutions']:
        a, b = entry[:2]
        print('- {0!r} -> {1!r}'.format(a, b))
        if len(entry) > 2:
            print('  ({0[2]})'.format(entry))
        re.compile(a)


def test_xrootd_replacement_targets():
    from os import listdir
    from os.path import join, isdir, basename
    data = json.load(open(MAP_FILE, 'rb'))

    def xrootd_paths():
        base = '/cvmfs/lhcb.cern.ch/lib/lcg/releases'
        for xrootd in [join(base, v, 'xrootd') for v in sorted(listdir(base))
                       if v.startswith('LCG_') and
                       isdir(join(base, v, 'xrootd'))]:
            for version in [v for v in listdir(xrootd)
                            if not v.startswith('.') and
                            isdir(join(xrootd, v))]:
                for platform in [p for p in listdir(join(xrootd, version))
                                 if not p.startswith('.') and
                                 isdir(join(xrootd, version, p))]:
                    yield join(xrootd, version, platform)

    print()
    for xrootd in xrootd_paths():
        value = xrootd
        for entry in data['substitutions']:
            a, b = entry[:2]
            value = re.sub(a, b, value)
        print(' - {0!r} -> {1!r}{2}'.format(
            xrootd, value, ' (no change)' if xrootd == value else ''))
        assert basename(xrootd) == basename(value), 'platform change'
        assert isdir(value), '{0} is not a directory'.format(value)


def test_xrootd_s20_versions():
    data = json.load(open(MAP_FILE, 'rb'))

    for xrootd, expected in [
        ('/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.1.0p2/x86_64-slc5-gcc43-opt/lib64', '/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.2.4/x86_64-slc5-gcc43-opt/lib64'),
        ('/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.2.4/x86_64-slc5-gcc46-opt/lib64', '/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.2.7/x86_64-slc5-gcc46-opt/lib64'),
        ('/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.1.0p2/x86_64-slc5-gcc46-opt/lib64', '/cvmfs/lhcb.cern.ch/lib/lcg/external/xrootd/3.2.7/x86_64-slc5-gcc46-opt/lib64'),
    ]:
        value = xrootd
        for entry in data['substitutions']:
            a, b = entry[:2]
            value = re.sub(a, b, value)
        print(' - {0!r} -> {1!r}{2}'.format(
            xrootd, value, ' (no change)' if xrootd == value else ''))
        assert expected in value.split(os.pathsep)


def test_compat_bin_lib():
    data = json.load(open(MAP_FILE, 'rb'))
    values = [
        [
            '/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v23r6/InstallArea/x86_64-slc5-gcc43-opt/lib',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/qt/4.7.4/x86_64-slc5-gcc43-opt/lib',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/pygraphics/1.3_python2.6/x86_64-slc5-gcc43-opt/lib',
            '/cvmfs/lhcb.cern.ch/lib/lcg/app/releases/RELAX/RELAX_1_3_0i/x86_64-slc5-gcc43-opt/lib',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/mysql/5.5.14/x86_64-slc5-gcc43-opt/lib'
        ],
        [
            '/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v23r6/InstallArea/x86_64-slc5-gcc43-opt/scripts',
            '/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v23r6/InstallArea/x86_64-slc5-gcc43-opt/bin',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/qt/4.7.4/x86_64-slc5-gcc43-opt/bin',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/pygraphics/1.3_python2.6/x86_64-slc5-gcc43-opt/bin',
            '/cvmfs/lhcb.cern.ch/lib/lcg/external/mysql/5.5.14/x86_64-slc5-gcc43-opt/bin'
        ],
    ]

    for platform in ['x86_64-slc5-gcc43-opt', 'x86_64-slc5-gcc43-dbg',
                     'x86_64-slc5-gcc34-opt', 'x86_64-slc5-icc11-opt']:
        for orig in values:
            orig = [o.replace('x86_64-slc5-gcc43-opt', platform) for o in orig]
            value = 'LD_LIBRARY_PATH=' + ':'.join(orig)
            is_bin = 'bin' in value
            for entry in data['substitutions']:
                a, b = entry[:2]
                value = re.sub(a, b, value)
            varname, value = value.split('=', 1)
            assert varname == 'LD_LIBRARY_PATH'
            value = value.split(':')
            if is_bin:
                assert len(value) == len(orig)
            else:
                expected = '{0}/InstallArea/{1}/lib'.format(
                    COMPAT_ROOT, platform.replace('dbg', 'opt'))
                assert len(value) == len(orig) + 1
                assert value[-1] == expected, '{0} not at end of {1}'.format(expected, ':'.join(value))
                assert value[:-1] == orig


def test_compat_conddbcompression():
    data = json.load(open(MAP_FILE, 'rb'))
    print()
    all_lhcb_versions = [
        v for v in os.listdir('/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB')
        if v.startswith('LHCB_v')]
    all_lhcb_versions.sort()
    overriden_versions = set(
        v for v in os.listdir(COMPAT_ROOT + '/CompatSys/CompressedDB')
        if v.startswith('LHCB_v'))
    for lhcb_version in all_lhcb_versions:
        if lhcb_version in ['LHCB_v32r4g2', 'LHCB_v34r4']:
            # CondDB compression already available
            continue
        platforms = [
            p for p in os.listdir('/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/' +
                                  lhcb_version + '/InstallArea')
            if '-' in p]
        for platform in platforms:
            to_override = lhcb_version in overriden_versions
            print(' - {0}/{1}{2}'.format(lhcb_version, platform,
                  '' if to_override else ' (no override)'))
            orig = ':'.join(
                ['{b}/LHCB/{v}/InstallArea/{p}/lib',
                 '{b}/GAUDI/GAUDI_v23r6/InstallArea/{p}/lib']
                ).format(v=lhcb_version, p=platform,
                         b='/cvmfs/lhcb.cern.ch/lib/lhcb')
            value = 'LD_LIBRARY_PATH=' + orig
            for entry in data['substitutions']:
                a, b = entry[:2]
                if 'CompressedDB' in b:
                    value = re.sub(a, b, value)
            varname, value = value.split('=', 1)
            assert varname == 'LD_LIBRARY_PATH'
            if to_override:
                value = value.split(':')
                expected = '{0}/CompatSys/CompressedDB/{1}/{2}'.format(
                    COMPAT_ROOT, lhcb_version, platform)
                assert len(value) == 3
                assert value[0] == expected, '{0} not at beginning of {1}'.format(expected, ':'.join(value))
                assert ':'.join(value[1:]) == orig
            else:
                assert value == orig, '{0} != {1}'.format(value, orig)


def test_compat_ignoreexitcode():
    data = json.load(open(MAP_FILE, 'rb'))
    print()
    all_lhcb_versions = [
        v for v in os.listdir('/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB')
        if v.startswith('LHCB_v')]
    all_lhcb_versions.sort()
    overriden_versions = set(
        v for v in os.listdir(COMPAT_ROOT + '/CompatSys/IgnoreExitCode')
        if v.startswith('LHCB_v'))
    overriden_versions.update(['LHCB_v39r1p1', 'LHCB_v39r1p3'])
    for lhcb_version in all_lhcb_versions:
        platforms = [
            p for p in os.listdir('/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/' +
                                  lhcb_version + '/InstallArea')
            if '-' in p]
        for platform in platforms:
            to_override = lhcb_version in overriden_versions and platform.endswith('-opt')
            print(' - {0}/{1}{2}'.format(lhcb_version, platform,
                  '' if to_override else ' (no override)'))
            orig = ':'.join(
                ['{b}/LHCB/{v}/InstallArea/{p}/lib',
                 '{b}/GAUDI/GAUDI_v23r6/InstallArea/{p}/lib']
                ).format(v=lhcb_version, p=platform,
                         b='/cvmfs/lhcb.cern.ch/lib/lhcb')
            value = 'LD_LIBRARY_PATH=' + orig
            for entry in data['substitutions']:
                a, b = entry[:2]
                if 'IgnoreExitCode' in b:
                    value = re.sub(a, b, value)
            varname, value = value.split('=', 1)
            assert varname == 'LD_LIBRARY_PATH'
            if to_override:
                if lhcb_version.startswith('LHCB_v39r1'):
                    lhcb_version = 'LHCB_v39r1'
                value = value.split(':')
                expected = '{0}/CompatSys/IgnoreExitCode/{1}/{2}'.format(
                    COMPAT_ROOT, lhcb_version, platform)
                assert len(value) == 3
                assert value[0] == expected, '{0} not at beginning of {1}'.format(expected, ':'.join(value))
                assert ':'.join(value[1:]) == orig
            else:
                assert value == orig, '{0} != {1}'.format(value, orig)


def test_hostos_replacement():
    data = json.load(open(MAP_FILE, 'rb'))
    cases = [('x86_64-centos7', 'x86_64-slc6-gcc49-opt', 'x86_64-slc6'),
             ('x86_64-centos7', 'x86_64+avx2+fma-slc6-gcc49-opt', 'x86_64-slc6'),
             ('x86_64-centos7', 'x86_64-centos7-gcc62-opt', 'x86_64-centos7'),
             ('x86_64-slc6', 'x86_64-slc6-gcc62-opt', 'x86_64-slc6'),
             ('x86_64-centos7', 'x86_64-slc5-gcc46-opt', 'x86_64-slc5'),
             ('x86_64-slc6', 'x86_64-slc5-gcc46-opt', 'x86_64-slc5')]

    value_template = ('LD_LIBRARY_PATH=blah/blah/test-x86_64-centos7/lib:'
                      '/cvmfs/.../gcc/.../{0}/lib64:/something:/stuff:'
                      '/cvmfs/.../project/version/{1}/lib:'
                      '/cvmfs/.../other/legacy/x86_64-slc5-gcc46-opt/lib')
    for host_os, platform, target_os in cases:
        value = value_template.format(host_os, platform)
        for entry in data['substitutions']:
            if len(entry) > 2 and 'host-os' in entry[2]:
                a, b = entry[:2]
                value = re.sub(a, b, value)
        assert value == value_template.format(target_os, platform)

    value_template = value_template.replace('LD_LIBRARY_PATH=', 'PATH=')
    for host_os, platform, target_os in cases:
        orig_value = value = value_template.format(host_os, platform)
        for entry in data['substitutions']:
            if len(entry) > 2 and 'host-os' in entry[2]:
                a, b = entry[:2]
                value = re.sub(a, b, value)
        assert value == orig_value
