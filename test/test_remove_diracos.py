import json
import os
import re

import pytest


MAP_FILE = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                        'override_map.json')

with open(MAP_FILE, "rt") as fp:
    map_data = json.load(fp)

# Taken from
#     lb-dirac v10r0 \
#           lb-run --siteroot=/cvmfs/lhcb.cern.ch/lib/ --allow-containers \
#           -c best  --use="ProdConf"   DaVinci/v51r0
VARABLES = """ANALYSISCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/AnalysisConf
ANALYSISPYTHONROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/AnalysisPython
ANALYSISROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Tutorial/Analysis
ANALYSISSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/AnalysisSys
ANALYSIS_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0
APPCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r395/options
APPCONFIGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AppConfig/v3r395
ARC_PLUGIN_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/lib64/arc
ASSOCIATORSBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Associators/AssociatorsBase
ASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Associators/Associators
BASH_ENV=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/bashrc
BBDECTREETOOLROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/BBDecTreeTool
BCMDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/BcmDet
BINARYDUMPERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/GPU/BinaryDumpers
BINARY_TAG=x86_64+avx2+fma-centos7-gcc9-opt
BINROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiHive/src/bin
CALOASSOCIATORSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Calo/CaloAssociators/options
CALOASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Calo/CaloAssociators
CALODAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Calo/CaloDAQ
CALODETOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/CaloDet/options
CALODETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/CaloDet
CALODETXMLCNVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/CaloDetXmlCnv
CALOFUTUREDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/CaloFuture/CaloFutureDAQ
CALOFUTUREINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/CaloFuture/CaloFutureInterfaces
CALOFUTUREMONIDSTOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFutureMoniDst/options
CALOFUTUREMONIDSTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFutureMoniDst
CALOFUTUREPIDSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFuturePIDs/options
CALOFUTUREPIDSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFuturePIDs
CALOFUTURERECOOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFutureReco/options
CALOFUTURERECOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFutureReco
CALOFUTURETOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/CaloFuture/CaloFutureTools
CALOFUTUREUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/CaloFuture/CaloFutureUtils
CALOINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Calo/CaloInterfaces
CALOKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Calo/CaloKernel
CALOMONIDSTOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloMoniDst/options
CALOMONIDSTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloMoniDst
CALOPIDSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloPIDs/options
CALOPIDSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloPIDs
CALORECOOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloReco/options
CALORECOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloReco
CALOTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Calo/CaloTools
CALOUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Calo/CaloUtils
CHARGEDPROTOANNPIDPARAMROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PARAM/ChargedProtoANNPIDParam/v1r7
CHARGEDPROTOANNPIDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/ChargedProtoANNPID
CMAKE_PREFIX_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb:/cvmfs/lhcb.cern.ch/lib/lcg/releases:/cvmfs/lhcb.cern.ch/lib/lcg/app/releases:/cvmfs/lhcb.cern.ch/lib/lcg/external:/cvmfs/lhcb.cern.ch/lib/contrib
CMTCONFIG=x86_64+avx2+fma-centos7-gcc9-opt
CMTPROJECTPATH=/cvmfs/lhcb.cern.ch/lib/lhcb:/cvmfs/lhcb.cern.ch/lib/lcg/releases:/cvmfs/lhcb.cern.ch/lib/lcg/app/releases:/cvmfs/lhcb.cern.ch/lib/lcg/external:/cvmfs/lhcb.cern.ch/lib/contrib
COMMONMCPARTICLESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/CommonMCParticles
COMMONPARTICLESARCHIVEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/CommonParticlesArchive
COMMONPARTICLESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/CommonParticles
DAQEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/DAQEvent
DAQKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/DAQKernel
DAQMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/DAQ/DAQMonitors
DAQSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/DAQSys
DAQUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/DAQUtils
DATA_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/data:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/share
DAVINCIANGLECALCULATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciAngleCalculators
DAVINCIASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciAssociators
DAVINCIDECAYFINDERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciDecayFinder
DAVINCIFILTERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciFilters
DAVINCIINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciInterfaces
DAVINCIKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciKernel
DAVINCIMCKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciMCKernel
DAVINCIMCTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciMCTools
DAVINCIMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciMonitors
DAVINCINEUTRALTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciNeutralTools
DAVINCIOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/Phys/DaVinci/options
DAVINCIOVERLAPSANDCLONESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciOverlapsAndClones
DAVINCIPVTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciPVTools
DAVINCIROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/Phys/DaVinci
DAVINCISYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/DaVinciSys
DAVINCITESTSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/DaVinciTests
DAVINCITOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciTools
DAVINCITRACKREFITTINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciTrackRefitting
DAVINCITRACKSCALINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DaVinciTrackScaling
DAVINCITRANSPORTERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciTransporter
DAVINCITYPESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciTypes
DAVINCIUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DaVinciUtils
DAVINCI_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0
DDDBROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DDDB
DECAYTREEFITTERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DecayTreeFitter
DECAYTREETUPLEANNPIDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleANNPID
DECAYTREETUPLEBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleBase
DECAYTREETUPLEDALITZROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleDalitz
DECAYTREETUPLEHERSCHELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleHerschel
DECAYTREETUPLEJETSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleJets
DECAYTREETUPLEMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleMC
DECAYTREETUPLEMUONCALIBROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleMuonCalib
DECAYTREETUPLERECOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleReco
DECAYTREETUPLEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTuple
DECAYTREETUPLETRACKINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleTracking
DECAYTREETUPLETRIGGERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DecayTreeTupleTrigger
DECFILESOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v31r1/options
DECFILESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v31r1
DETCONDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetCond
DETDESCCHECKSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetDescChecks
DETDESCCNVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetDescCnv
DETDESCEXAMPLEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Ex/DetDescExample
DETDESCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetDesc
DETDESCSVCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetDescSvc
DETSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/DetSys
DIGIEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/DigiEvent
DIRAC=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro
DIRACOS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos
DIRACPLAT=Linux_x86_64_glibc-2.17
DIRACSCRIPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/scripts
DISPLVERTICESMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/DisplVerticesMC
DISPLVERTICESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/DisplVertices
DOXYGENROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiRelease/doc/doxygen
DSTWRITERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/DSTWriters
DUMMYPRODUCERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/DummyProducers
EDITOR=/bin/nano -w
EVENTASSOCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/EventAssoc
EVENTBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/EventBase
EVENTPACKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/EventPacker
EVENTSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/EventSys
EXTRAINFOTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/ExtraInfoTools
FASTPVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tf/FastPV
FIELDMAP=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/FieldMap/v5r7/cdf/field047.cdf
FIELDMAPROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/FieldMap/v5r7
FILESTAGERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Tools/FileStager
FLAVOURTAGGINGCHECKEROPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/FlavourTaggingChecker/options
FLAVOURTAGGINGCHECKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/FlavourTaggingChecker
FLAVOURTAGGINGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/FlavourTagging/options
FLAVOURTAGGINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/FlavourTagging
FSRALGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/FSRAlgs
FSREVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/FSREvent
FTDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/FT/FTDAQ
FTDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/FTDet
FTEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/FTEvent
FUNCTORCACHEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Phys/FunctorCache
FUNCTORCOREROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Phys/FunctorCore
GAUDIALGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiAlg
GAUDIAPPNAME=DaVinci
GAUDIAPPVERSION=v51r0
GAUDIAUDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiAud
GAUDICOMMONSVCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiCommonSvc
GAUDICONFIGURATIONROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiConfiguration
GAUDICONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/GaudiConf
GAUDICONFUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/GaudiConfUtils
GAUDICORESVCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiCoreSvc
GAUDIEXAMPLESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiExamples
GAUDIGSLROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/GaudiGSL
GAUDIHIVEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiHive
GAUDIKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiKernel
GAUDIMONITORROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiMonitor
GAUDIMPROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiMP
GAUDIPARTPROPROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiPartProp
GAUDIPLUGINSERVICEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiPluginService
GAUDIPOLICYROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiPolicy
GAUDIPROFILINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiProfiling
GAUDIPYTHONROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiPython
GAUDIRELEASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiRelease
GAUDIROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/Gaudi
GAUDISVCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiSvc
GAUDITENSORFLOWROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Tools/GaudiTensorFlow
GAUDIUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiUtils
GAUDI_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0
GAUDI_QMTEST_CLASS=LHCbTest
GAUDI_QMTEST_MODULE=GaudiConf.QMTest.LHCbTest
GENERICVERTEXFINDERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/GenericVertexFinder
GENEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/GenEvent
GFAL_CONFIG_DIR=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/etc/gfal2.d
GFAL_PLUGIN_DIR=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/lib64/gfal2-plugins
GITENTITYRESOLVERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Tools/GitEntityResolver
GLOBALRECOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/GlobalReco
GLOBUS_FTP_CLIENT_IPV6=TRUE
GLOBUS_IO_IPV6=TRUE
HCDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/HC/HCDAQ
HCMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/HC/HCMonitors
HIGHPTJETSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/HighPtJets
HLTDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Hlt/HltDAQ
HLTEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/HltEvent
HLTINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/HltInterfaces
HLTSCHEDULERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Hlt/HLTScheduler
HLTSELCHECKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Hlt/HltSelChecker
HLTSERVICESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Hlt/HltServices
HLTTCKROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TCK/HltTCK/v3r19p28
HOME=/afs/cern.ch/user/c/cburr
HOSTNAME=lxplus775.cern.ch
INCTOPOVERTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/IncTopoVert
IOEXAMPLEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Ex/IOExample
ISOLATIONTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/IsolationTools
JETACCESSORIESMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/JetAccessoriesMC
JETACCESSORIESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/JetAccessories
JETTAGGINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/JetTagging
KALMANFILTERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/KalmanFilter
KERASROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/keras
KERNELSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/KernelSys
KRB5CCNAME=FILE:/tmp/krb5cc_73370_fc3T894Kzz
L0BASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0Base
L0CALOOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0Calo/options
L0CALOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0Calo
L0DUOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0DU/options
L0DUROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0DU
L0EVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/L0Event
L0HCOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0HC/options
L0HCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0HC
L0INTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0Interfaces
L0MUONKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0MuonKernel
L0MUONROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/L0Muon
L0TCK=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TCK/L0TCK/v5r32/options
L0TCKROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TCK/L0TCK/v5r32
LANG=en_GB.UTF-8
LBCOMSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/LbcomSys
LBCOM_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0
LCG_releases_base=/cvmfs/lhcb.cern.ch/lib/lcg/releases
LC_CTYPE=UTF-8
LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/HepMC/2.06.10/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/9.2.0-afc57/x86_64-centos7/lib64:/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Vc/1.4.1/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/fftw3/3.3.8/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/fastjet/3.3.2/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/XercesC/3.1.3/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/GSL/2.5/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/vdt/0.4.3/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/clhep/2.4.1.2/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/tbb/2019_U7/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/neurobayes_expert/3.7.0/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/blas/0.3.5.openblas/x86_64-centos7-gcc9-opt/lib64:/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/RELAX/root6/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/catboost/0.9.1.1/x86_64-centos7-gcc9-opt/catboost/libs/model_interface:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/xgboost/0.90/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/libgit2/0.28.2-5bc7a/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/lapack/3.8.0/x86_64-centos7-gcc9-opt/lib64:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/tensorflow/1.14.0/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/CppUnit/1.14.0/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/jemalloc/5.2.0/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/libunwind/1.3.1/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/gperftools/2.7/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/png/1.6.37/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/zlib/1.2.11/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/HepPDT/2.06.01/x86_64-centos7-gcc9-opt/lib
LHCBALGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/LHCbAlgs
LHCBKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/LHCbKernel
LHCBMATHROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/LHCbMath
LHCBSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/LHCbSys
LHCBTRACKINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Tr/LHCbTrackInterfaces
LHCB_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0
LINKEREVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/LinkerEvent
LINKERINSTANCESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/LinkerInstances
LOKIALGOMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/LoKiAlgoMC
LOKIALGOROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiAlgo
LOKIARRAYFUNCTORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiArrayFunctors
LOKICOREROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Phys/LoKiCore
LOKIEXAMPLEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Ex/LoKiExample
LOKIFITTERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiFitters
LOKIGENMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/LoKiGenMC
LOKIGENROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Phys/LoKiGen
LOKIHLTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Phys/LoKiHlt
LOKIJETSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiJets
LOKIMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Phys/LoKiMC
LOKINUMBERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Phys/LoKiNumbers
LOKIPHYSMCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/LoKiPhysMC
LOKIPHYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiPhys
LOKIPROTOPARTICLESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiProtoParticles
LOKIROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKi
LOKITRACKROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/LoKiTrack
LOKITRACKSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiTracks
LOKITRACK_V2ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/LoKiTrack_v2
LOKIUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/LoKiUtils
LUMIALGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/LumiAlgs
LUMIEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/LumiEvent
LUMINOSITYROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/Luminosity
LWTNNPARSERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/lwtnnParser
MAGNETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/Magnet
MCASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Associators/MCAssociators
MCEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/MCEvent
MCINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/MCInterfaces
MDFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/MDF
MDF_ROOTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/MDF_ROOT
MICRODSTALGORITHMROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTAlgorithm
MICRODSTBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTBase
MICRODSTCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTConf
MICRODSTINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTInterfaces
MICRODSTOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTConf/options
MICRODSTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/MicroDst
MICRODSTTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/MicroDST/MicroDSTTools
MUONASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Muon/MuonAssociators
MUONDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Muon/MuonDAQ
MUONDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/MuonDet
MUONIDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonID
MUONINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonInterfaces
MUONKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Muon/MuonKernel
MUONMATCHROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonMatch
MUONPIDCHECKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonPIDChecker
MUONTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonTools
MUONTRACKALIGNROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonTrackAlign
MUONTRACKMONITORROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonTrackMonitor
MUONTRACKRECROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Muon/MuonTrackRec
MVADICTTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/MVADictTools
MYSITEROOT=/cvmfs/lhcb.cern.ch/lib
NEUROBAYESTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/NeuroBayesTools
OMP_THREAD_LIMIT=1
OPENBLAS_NUM_THREADS=1
OPENSSL_CONF=/tmp
OSTAPROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Analysis/Ostap
OSTAPTUTORROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Tutorial/OstapTutor
OTDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/OT/OTDAQ
OTDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/OTDet
PARAMETERIZEDKALMANROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/ParameterizedKalman
PARAMFILESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PARAM/ParamFiles/v8r29
PARTICLE2MCTRUTHROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/Particle2MCTruth
PARTICLECOMBINERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/ParticleCombiners
PARTICLECONVERTERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/ParticleConverters
PARTICLEMAKERCHECKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/ParticleMakerChecker
PARTICLEMAKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/ParticleMaker
PARTPROPROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/PartProp
PARTPROPSVCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/PartPropSvc
PATCHECKERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/PatChecker
PATFITPARAMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/PatFitParams
PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/9.2.0-afc57/x86_64-centos7/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts:/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts:/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/pyqt5/5.12.3/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/gdb/8.3/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/nose/1.3.7/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/doxygen/1.8.15/x86_64-centos7-gcc9-opt/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/scripts:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/bin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/sbin:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/sbin:/usr/local/bin:/usr/bin
PATPVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/PatPV
PHYSCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/PhysConf
PHYSDICTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/PhysDict
PHYSEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/PhysEvent
PHYSINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/PhysInterfaces
PHYSSELPYTHONROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/PhysSel/PhysSelPython
PHYSSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/PhysSys
PHYS_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0
PRALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrAlgorithms
PRCONFIGOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/PRConfig/v1r43/options
PRCONFIGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/PRConfig/v1r43
PRCONVERTERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrConverters
PRFITPARAMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrFitParams
PRKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrKernel
PRMCTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrMCTools
PROCESSORKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/L0/ProcessorKernel
PRODCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/ProdConf/v3r1
PROTOPARTICLEFILTERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/ProtoParticleFilter
PRPIXELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrPixel
PRVELOUTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrVeloUT
PRVPRETINAROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/PrVPRetina
PS1=[DaVinci v51r0] >
PWD=/afs/cern.ch/user/c/cburr
PYTHONHOME=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc9-opt
PYTHONOPTIMIZE=x
PYTHONPATH=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/ProdConf/v3r1/python:/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TurboStreamProd/v4r2p10/python:/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/enum34/1.1.6/x86_64-centos7-gcc9-opt/lib/python2.7/site-packages:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/pyqt5/5.12.3/x86_64-centos7-gcc9-opt/lib/python2.7/site-packages:/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/RawEventFormat/v1r9/python:/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/PRConfig/v1r43/python:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python/lib-dynload:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/python.zip:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro
PYTHONUNBUFFERED=yes
PYTHONWARNINGS=ignore
QMTESTFILESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PARAM/QMTestFiles/v1r3
QMTEST_CLASS_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/qmtest_classes
RAWEVENTCOMPATROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/RawEventCompat
RAWEVENTFORMATROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/RawEventFormat/v1r9
RECALGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/RecAlgs
RECCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/RecConf
RECEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/RecEvent
RECINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rec/RecInterfaces
RECREATEPIDTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/RecreatePIDTools
REC_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0
RELATEDINFOTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/RelatedInfoTools
RELATIONSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/Relations
RICHALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichAlgorithms
RICHDAQKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichDAQKernel
RICHDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichDAQ
RICHDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/RichDet
RICHFUTUREALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichFutureAlgorithms
RICHFUTUREDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichFutureDAQ
RICHFUTUREGLOBALPIDROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureGlobalPID
RICHFUTUREKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichFutureKernel
RICHFUTUREMCUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichFutureMCUtils
RICHFUTURERECALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecAlgorithms
RICHFUTURERECBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecBase
RICHFUTURERECCHECKERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecCheckers
RICHFUTURERECEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecEvent
RICHFUTURERECINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecInterfaces
RICHFUTURERECMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecMonitors
RICHFUTURERECPHOTONALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecPhotonAlgorithms
RICHFUTURERECPIXELALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecPixelAlgorithms
RICHFUTURERECSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecSys
RICHFUTURERECTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecTools
RICHFUTURERECTRACKALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichFutureRecTrackAlgorithms
RICHFUTURETOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichFutureTools
RICHFUTUREUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichFutureUtils
RICHINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichInterfaces
RICHKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichKernel
RICHMCMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichMCMonitors
RICHMCTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichMCTools
RICHPIDQCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Rich/RichPIDQC
RICHRECTESTSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichRecTests
RICHRECUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Rich/RichRecUtils
RICHTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Rich/RichTools
RICHUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Rich/RichUtils
ROOTCNVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/RootCnv
ROOTDUMPERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/GPU/RootDumpers
ROOTHISTCNVROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/RootHistCnv
ROOTSYS=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt
ROOT_INCLUDE_PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/clhep/2.4.1.2/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/fastjet/3.3.2/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/HepMC/2.06.10/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/AIDA/3.2.1/x86_64-centos7-gcc9-opt/src/cpp:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc9-opt/include/python2.7:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/vdt/0.4.3/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/GSL/2.5/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/cppgsl/2.0.0/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/eigen/3.3.7/x86_64-centos7-gcc9-opt/include/eigen3:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/xgboost/0.90/x86_64-centos7-gcc9-opt/rabit/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/xgboost/0.90/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/rangev3/0.5.0/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/XercesC/3.1.3/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/CppUnit/1.14.0/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/HepPDT/2.06.01/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/gperftools/2.7/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/zlib/1.2.11/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/libunwind/1.3.1/x86_64-centos7-gcc9-opt/include:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/tbb/2019_U7/x86_64-centos7-gcc9-opt/include
RRD_DEFAULT_FONT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/share/fonts/DejaVuSansMono-Roman.ttf
SCIFITRACKFORWARDINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Pr/SciFiTrackForwarding
SELALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Phys/SelAlgorithms
SELECTIONLINEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Kernel/SelectionLine
SELKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Phys/SelKernel
SELPYROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/SelPy
SELREPORTSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/SelReports
SELTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Phys/SelTools
SHLVL=1
SIDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Si/SiDAQ
SIMCOMPONENTSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Sim/SimComponents
SITOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/Si/SiTools
SOACONTAINERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/SOAContainer
SOAEXTENSIONSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/SOAExtensions
SSL_CERT_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/certificates
STANDARDPARTICLESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StandardParticles
STDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/ST/STDAQ
STDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/STDet
STDOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/GaudiConf/options
STKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/ST/STKernel
STRIPPINGALGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingAlgs
STRIPPINGARCHIVEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingArchive
STRIPPINGCACHEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/Phys/StrippingCache
STRIPPINGCONFROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingConf
STRIPPINGDOCROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingDoc
STRIPPINGNEUROBAYESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingNeuroBayes
STRIPPINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/Stripping
STRIPPINGSELECTIONSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingSelections
STRIPPINGSETTINGSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingSettings
STRIPPINGSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/StrippingSys
STRIPPINGTCKROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingTCK
STRIPPINGUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/StrippingUtils
STRIPPING_PROJECT_ROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2
STTELL1EVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/ST/STTELL1Event
SWIMMINGEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/SwimmingEvent
SWIMMINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/Swimming
TELL1KERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/DAQ/Tell1Kernel
TERM=xterm-256color
TERMINFO=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/share/terminfo:/usr/share/terminfo:/etc/terminfo
TESLAROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/Phys/Tesla
TESLATOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/TeslaTools
TFKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tf/TfKernel
TISTOSTOBBINGROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/TisTosTobbing
TMPDIR=/tmp/cburr
TMVASELECTIONSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/Phys/TMVASelections
TMVAWEIGHTSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PARAM/TMVAWeights/v1r16
TOPOLOGICALTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/TopologicalTools
TRACKASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackAssociators
TRACKCHECKERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackCheckers
TRACKEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/TrackEvent
TRACKEXTRAPOLATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackExtrapolators
TRACKFITEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackFitEvent
TRACKFITTERROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackFitter
TRACKIDEALPRROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackIdealPR
TRACKINTERFACESROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackInterfaces
TRACKKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackKernel
TRACKMCTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackMCTools
TRACKMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackMonitors
TRACKPROJECTORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackProjectors
TRACKSYSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tf/TrackSys
TRACKTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackTools
TRACKUTILSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tr/TrackUtils
TSAALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tf/TsaAlgorithms
TSAKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/Tf/TsaKernel
TURBOCACHEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v51r0/Phys/TurboCache
TURBOLINES=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TurboStreamProd/v4r2p10/python/TurboStreamProd
TURBOSTREAMPRODROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/TurboStreamProd/v4r2p10
USER=cburr
UTALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTAlgorithms
UTASSOCIATORSOPTS=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTAssociators/options
UTASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTAssociators
UTCHECKERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTCheckers
UTDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/UT/UTDAQ
UTDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/UTDet
UTILROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/GaudiKernel/src/Util
UTKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/UT/UTKernel
UTMONITORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTMonitors
UTTELL1EVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/UT/UTTELL1Event
UTTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/UT/UTTools
VELODAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Velo/VeloDAQ
VELODETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/VeloDet
VELOEVENTROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Event/VeloEvent
VERTEXFITCHECKROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/Phys/VertexFitCheck
VERTEXFITROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/Phys/VertexFit
VOMS_USERCONF=/cvmfs/lhcb.cern.ch/etc/grid-security/vomses
VPALGORITHMSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/VP/VPAlgorithms
VPASSOCIATORSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/VP/VPAssociators
VPCHECKERSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/VP/VPCheckers
VPDAQROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/VP/VPDAQ
VPDETROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Det/VPDet
VPKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/VP/VPKernel
VPTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v31r0/VP/VPTools
X509_CERT_DIR=/etc/grid-security/certificates
X509_VOMSES=/cvmfs/lhcb.cern.ch/etc/grid-security/vomses
X509_VOMS_DIR=/etc/grid-security/vomsdir
XMLSUMMARYBASEROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/XMLSummaryBase
XMLSUMMARYKERNELROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Kernel/XMLSummaryKernel
XMLTOOLSROOT=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/Tools/XmlTools
XRD_RUNFORKHANDLER=1""".split('\n')


def _apply_substitutions(string):
    result = string
    for entry in map_data['substitutions']:
        a, b = entry[:2]
        result = re.sub(a, b, result)
    return result


def test_var_diracos():
    string = "DIRACOS=/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos"
    expected_result = string
    result = _apply_substitutions(string)
    assert result == expected_result


def test_var_path():
    paths = [
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/9.2.0-afc57/x86_64-centos7/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r2/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/pyqt5/5.12.3/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v31r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v51r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/gdb/8.3/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/nose/1.3.7/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v33r0/InstallArea/x86_64+avx2+fma-centos7-gcc9-opt/scripts"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/doxygen/1.8.15/x86_64-centos7-gcc9-opt/bin"),
        (True, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/scripts"),
        (False, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/bin"),
        (False, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/bin"),
        (False, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/sbin"),
        (False, "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCBDIRAC/pro/diracos/usr/sbin"),
        (True, "/usr/local/bin"),
        (True, "/usr/bin"),
    ]
    string = "PATH=" + ":".join([p for b, p in paths])
    expected_result = "PATH=" + ":".join([p for b, p in paths if b])
    result = _apply_substitutions(string)
    assert result == expected_result


@pytest.mark.parametrize("string", VARABLES)
def test_real_env(string):
    result = _apply_substitutions(string)

    if '/diracos/' in string:
        assert 'diracos' not in result
        assert '/diracos/' not in result
        assert '::' not in result
    else:
        assert string == result
