#!/usr/bin/env python
#
# Test that checks all versions of LHCb installed on CVMFS, to see if a
# specific file can be copied locally using xrdcp.
#
# This should be invoked using "nosetest"
#

from __future__ import print_function

import json
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from tempfile import mkstemp

import pytest

# Hard coded path to the list of LHCb versions
top_path = "/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB"

try:
    subprocess.check_call('klist')
except subprocess.CalledProcessError:
    KRB_CREDENTIALS_AVAILABLE = False
else:
    KRB_CREDENTIALS_AVAILABLE = True


def listLHCbVersions():
    ''' Return the list of LHCb versions installed in the top_path '''
    # Listing the versions of LHCb we want to test
    # We take all from CVMFS
    lhcb_paths = [t.split("_") for t in os.listdir(top_path) if "LHCB" in t]
    return [v for (p, v) in lhcb_paths]


def listPlatforms(version):
    '''  List the platforms installed for a specific version of LHCb '''
    excluded = ["include", "python", "scripts"]
    iapath = os.path.join(top_path, "LHCB_%s" % version, "InstallArea")
    return [p for p in os.listdir(iapath) if p not in excluded
            and (p.startswith("sl") or p.startswith("x"))
            and 'icc' not in p]


class CheckXrdcp(object):
    ''' Check a specific version of LHCb, for a given platform '''
    def __init__(self, version, platform):
        self.description = 'Checking LHCb/%s %s' % (version, platform)
        self.version = version
        self.platform = platform

    def __call__(self):
        print(self.description)

        # Setting a needed env variable
        # os.environ["XrdSecPROTOCOL"]="gsi,unix"

        (outfd, outtempfile) = mkstemp('checkxrootd.json')
        # We remove it as xrdcp does not like it there and the --force option
        # did not always exist...
        try:
            os.remove(outtempfile)
        except Exception:
            pass

        try:
            command = ['lb-run', '-c', self.platform, 'LHCb/%s' % self.version,
                       'xrdcp', 'root://eoslhcb.cern.ch:1094//eos/lhcb/grid/user/lhcb/user/b/bcouturi/test.json',
                       outtempfile]

            out = Popen(command, stderr=STDOUT, stdout=PIPE)
            log, retcode = out.communicate()[0], out.returncode
            if retcode == 0:
                # If we got the file, we try and read it...
                print("Opening: ", outtempfile)
                with open(outtempfile) as f:
                    try:
                        res = json.load(f)
                        content = res['TEST']
                        if content != 'Success':
                            log += "\nBad file content: %s" % content
                            retcode = 1
                    except Exception:
                        log += "\nCould not read opened file."
                        retcode = 1

        finally:
            try:
                # Ignore remopval problem, maybe the file was never created...
                os.remove(outtempfile)
            except Exception:
                None

        if retcode != 0:
            print("Error:", log)
        assert retcode == 0


def get_params():
    ''' Test method that generates the tests for all versions and platforms '''
    from LbPlatformUtils.describe import allBinaryTags
    from LbPlatformUtils import requires, can_run, dirac_platform
    host = dirac_platform()
    supported_platforms = [btag for btag in allBinaryTags()
                           if can_run(host, requires(btag))]

    versions = listLHCbVersions()
    for version in versions[::-1]:
        if version == 'v42r0':
            # skip LHCb 'v42r0' because of a deployment issue (wrong .xenv)
            continue
        platforms = listPlatforms(version)
        for platform in platforms:
            if platform in supported_platforms:
                yield version, platform
            else:
                print("Platform %s is not suported" % platform)


@pytest.mark.parametrize("version, platform", list(get_params()))
@pytest.mark.skipif(not KRB_CREDENTIALS_AVAILABLE, reason="klist did not show any credentials")
def test_xrootd(version, platform):
    CheckXrdcp(version, platform)()
